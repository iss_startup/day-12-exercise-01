//Load express
var express = require("express");

//Create an instance of express application
var app = express();
var bodyParser = require("body-parser");

//TODO : Import Sequelize <Here>


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

//TODO :Create a connection with mysql DB <HERE>
var MYSQL_USERNAME = '';
var MYSQL_PASSWORD = '';

var conn = new Sequelize('employees', MYSQL_USERNAME, MYSQL_PASSWORD, {

});

// var Department = require('./model/department')(conn, Sequelize);
// var Manager = require('./model/manager')(conn, Sequelize);

//TODO : create route handlers to execute sequelize queries to search department by ID, delete department, edit department

Department.hasMany(Manager,{foreignKey : 'dept_no'});
app.get("/api/departments", function (req, res) {
    var where = {};

    if (req.query.dept_name) {
        where.dept_name = {
            $like: "%" + req.query.dept_name + "%"
        }
    }

    if(req.query.dept_no){
        where.dept_no = req.query.dept_no;
    }

    console.log("where: " + JSON.stringify(where));
    Department
        .findAll({
            where: where
        })
        .then(function (departments) {
            res.json(departments);
        })
        .catch(function () {
            res
                .status(500)
                .json({error: true})
        });

});
app.get("/api/departmentManager", function (req, res) {
    var where = {};

    console.log("query: " + JSON.stringify(req.query));
    if (req.query.dept_name) {
        where.dept_name = {
            $like: "%" + req.query.dept_name + "%"
        }
    }

    if(req.query.dept_no){
        where.dept_no = req.query.dept_no;
    }

    Department
        .findAll({
            where: where,
            include : [
                Manager
            ]
        })
        .then(function (departments) {
            res.json(departments);
        })
        .catch(function (err) {
            console.log("error " + err);
            res
                .status(500)
                .json({error: true})
        });

});
app.post("/api/departments", function (req, res) {
    console.log("Query", req.query);
    console.log("Body", req.body);

    Department
        .create({
            dept_no: req.body.dept_no,
            dept_name: req.body.dept_name
        })
        .then(function (department) {
            res
                .status(200)
                .json(department);
        })
        .catch(function (err) {
            console.log("error: " + err);
            res
                .status(500)
                .json({error: true});
        })


});

//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});
